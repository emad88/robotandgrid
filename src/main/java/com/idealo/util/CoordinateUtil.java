package com.idealo.util;

import com.idealo.datatransferobject.RobotDTO;
import com.idealo.domainvalue.Facing;
import com.idealo.service.RobotServiceImpl;
import org.slf4j.LoggerFactory;

public final class CoordinateUtil {

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(RobotServiceImpl.class);
    private final static int MIN = 0;
    private final static int MAX = 4;
    private final static int MOVE_BY_CELL = 1;


    public static RobotDTO moveRobot(RobotDTO robotDTO) {
        switch (robotDTO.getFacing()) {
            case EAST:
                return moveEast(robotDTO);
            case WEST:
                return moveWest(robotDTO);
            case NORTH:
                return moveNorth(robotDTO);
            case SOUTH:
                return moveSouth(robotDTO);
        }

        return robotDTO;
    }

    public static RobotDTO turnLeft(RobotDTO robotDTO){
        switch (robotDTO.getFacing()){
            case EAST:
                robotDTO.setFacing(Facing.NORTH);
                break;
            case WEST:
                robotDTO.setFacing(Facing.SOUTH);
                break;
            case SOUTH:
                robotDTO.setFacing(Facing.EAST);
                break;
            case NORTH:
                robotDTO.setFacing(Facing.WEST);
                break;
        }
        return robotDTO;
    }

    public static RobotDTO turnRight(RobotDTO robotDTO){
        switch (robotDTO.getFacing()){
            case EAST:
                robotDTO.setFacing(Facing.SOUTH);
                break;
            case WEST:
                robotDTO.setFacing(Facing.NORTH);
                break;
            case SOUTH:
                robotDTO.setFacing(Facing.WEST);
                break;
            case NORTH:
                robotDTO.setFacing(Facing.EAST);
                break;
        }

        return robotDTO;
    }


    private static RobotDTO moveNorth(RobotDTO robotDTO) {
        int yPlus = robotDTO.getY() + MOVE_BY_CELL;
        if (isMoveAllowed(yPlus))
            robotDTO.setY(yPlus);
        return robotDTO;
    }

    private static RobotDTO moveSouth(RobotDTO robotDTO) {
        int yMinus = robotDTO.getY() - MOVE_BY_CELL;
        if (isMoveAllowed(yMinus))
            robotDTO.setY(yMinus);
        return robotDTO;
    }

    private static RobotDTO moveWest(RobotDTO robotDTO) {
        int xMinus = robotDTO.getX() - MOVE_BY_CELL;
        if (isMoveAllowed(xMinus))
            robotDTO.setX(xMinus);
        return robotDTO;
    }

    private static RobotDTO moveEast(RobotDTO robotDTO) {
        int xPlus = robotDTO.getX() + MOVE_BY_CELL;
        if (isMoveAllowed(xPlus))
            robotDTO.setX(xPlus);
        return robotDTO;
    }

    private static boolean isMoveAllowed(int nextCell) {
        if (nextCell < MIN || nextCell > MAX)
            return false;
        return true;
    }
}
