package com.idealo.service;

import com.idealo.datatransferobject.RobotDTO;
import com.idealo.exception.EntityNotFoundException;
import com.idealo.util.CoordinateUtil;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.ArrayList;

@Service
public class RobotServiceImpl implements RobotService {

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(RobotServiceImpl.class);

    private static List<RobotDTO> robots = new ArrayList<>();
    private static int robotCount = 0;

    /**
     * Places the robot on the grid
     * @param robotDTO
     * @return robotDTO
     */
    @Override
    public RobotDTO placeRobot(RobotDTO robotDTO) {
        if (robotDTO.getId() == null)
            robotDTO.setId(++robotCount);
        robots.add(robotDTO);
        return robotDTO;
    }

    /**
     * moves the robot by one cell to the direction it's facing
     * @param id
     * @throws EntityNotFoundException
     */
    @Override
    public void moveRobot(int id) throws EntityNotFoundException {
        int index = robotIndex(id);
        RobotDTO robotDTO = robots.get(index);
        robots.set(index, CoordinateUtil.moveRobot(robotDTO));
        LOG.debug("id: {}, moveRobot robot: {}", id, robots.get(index));
    }

    /**
     * turns the robot 90 degrees to its left
     * @param id
     * @throws EntityNotFoundException
     */
    @Override
    public void turnLeft(int id) throws EntityNotFoundException {
        int index = robotIndex(id);
        RobotDTO robotDTO = robots.get(index);
        robots.set(index, CoordinateUtil.turnLeft(robotDTO));
        LOG.debug("id: {}, turnLeft robot: {}", id, robots.get(index));
    }

    /**
     * moves the robot 90 degrees to its right
     * @param id
     * @throws EntityNotFoundException
     */
    @Override
    public void turnRight(int id) throws EntityNotFoundException {
        int index = robotIndex(id);
        RobotDTO robotDTO = robots.get(index);
        robots.set(index, CoordinateUtil.turnRight(robotDTO));
        LOG.debug("id: {},turnRight robot: {}", id, robots.get(index));
    }

    /**
     * prints the robot's current position and facing
     * @param id
     * @return robotDTO
     * @throws EntityNotFoundException
     */
    @Override
    public RobotDTO report(int id) throws EntityNotFoundException {
        Integer index = robotIndex(id);
        return robots.get(index);
    }

    /**
     * finds the index of specified robot
     * @param id
     * @return index of found robot in the list
     * @throws EntityNotFoundException
     */
    private int robotIndex(int id) throws EntityNotFoundException {
        for (int i = 0; i < robots.size(); i++) {
            if (robots.get(i).getId() == id) {
                return i;
            }
        }
        throw new EntityNotFoundException("Robot with id is not found (Robot Missing)");
    }
}
