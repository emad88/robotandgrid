package com.idealo.service;

import com.idealo.datatransferobject.RobotDTO;
import com.idealo.exception.EntityNotFoundException;

public interface RobotService {

    RobotDTO placeRobot(RobotDTO robotDTO);

    RobotDTO report(int id) throws EntityNotFoundException;

    void moveRobot(int id) throws EntityNotFoundException;

    void turnLeft(int id) throws EntityNotFoundException;

    void turnRight(int id) throws EntityNotFoundException;
}
