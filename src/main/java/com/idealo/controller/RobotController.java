package com.idealo.controller;

import com.idealo.datatransferobject.RobotDTO;
import com.idealo.exception.EntityNotFoundException;
import com.idealo.service.RobotService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("v1/robot")
public class RobotController {

    private final RobotService robotService;

    public RobotController(RobotService robotService) {
        this.robotService = robotService;
    }

    /**
     * place the robot on the grid
     * @param robotDTO
     * @return the created robot id in the response header location
     */
    @PostMapping
    private ResponseEntity<Object> placeRobotOnGrid(@Valid @RequestBody RobotDTO robotDTO) {
        RobotDTO placedRobot = robotService.placeRobot(robotDTO);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(placedRobot.getId())
                .toUri();
        return ResponseEntity.created(location).build();
    }

    /**
     * moves the robot by one cell
     * @param id
     * @throws EntityNotFoundException
     */
    @PutMapping("/{id}")
    public void moveRobot(@Valid @PathVariable int id) throws EntityNotFoundException {
        robotService.moveRobot(id);
    }

    /**
     * turns the robot 90 degress to the left of its current facing
     * @param id
     * @throws EntityNotFoundException
     */
    @PutMapping("turnleft/{id}")
    public void turnLeft(@Valid @PathVariable int id) throws EntityNotFoundException {
        robotService.turnLeft(id);
    }

    /**
     * turns the robot 90 degress to the right of its current facing
     * @param id
     * @throws EntityNotFoundException
     */
    @PutMapping("turnright/{id}")
    public void turnRight(@Valid @PathVariable int id) throws EntityNotFoundException {
        robotService.turnRight(id);
    }

    /**
     * report the current position of robot and its facing
     * @param id
     * @return RobotDTO
     * @throws EntityNotFoundException
     */
    @GetMapping("/{id}")
    public RobotDTO reportRobot(@Valid @PathVariable int id) throws EntityNotFoundException {
        return robotService.report(id);
    }


}
