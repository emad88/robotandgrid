package com.idealo.datatransferobject;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.idealo.domainvalue.Facing;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RobotDTO {

    @JsonIgnore
    private Integer id;
    @Min(value = 0, message = "minimum value of x is 0")
    @Max(value = 4, message = "maximum value of x is 4")
    @NotNull(message = "x value should not null")
    private int x;
    @Min(value = 0, message = "minimum value of y is 0")
    @Max(value = 4, message = "maximum value of y is 4")
    @NotNull(message = "y value should not null")
    private int y;
    @NotNull(message = "facing should not be null")
    private Facing facing;
}
