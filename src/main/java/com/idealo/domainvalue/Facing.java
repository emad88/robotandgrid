package com.idealo.domainvalue;

public enum Facing {
    NORTH, SOUTH, EAST, WEST
}
