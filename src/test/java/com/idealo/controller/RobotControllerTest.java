package com.idealo.controller;

import com.idealo.RobotandgridApplication;
import com.idealo.datatransferobject.RobotDTO;
import com.idealo.domainvalue.Facing;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RobotandgridApplication.class)
@ComponentScan("com.idealo")
@EnableAutoConfiguration
@WebAppConfiguration
public class RobotControllerTest {

    private static final Logger log = LoggerFactory.getLogger(RobotController.class);

    private static final String URI = "/v1/robot";


    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @SuppressWarnings("rawtypes")
    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
                .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
                .findAny()
                .orElse(null);

        assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() throws Exception {
        mockMvc = webAppContextSetup(webApplicationContext).build();

        RobotDTO robotDTO = new RobotDTO();
        robotDTO.setFacing(Facing.NORTH);
        robotDTO.setX(0);
        robotDTO.setY(0);
        log.debug("robotDTO {}", this.json(robotDTO));

        postRequest(robotDTO); // id 1

        robotDTO.setFacing(Facing.NORTH);
        robotDTO.setX(0);
        robotDTO.setY(0);
        postRequest(robotDTO); // id 2

        robotDTO.setFacing(Facing.EAST);
        robotDTO.setX(1);
        robotDTO.setY(2);
        postRequest(robotDTO); // id 3
    }

    @Test
    public void testCase1() throws Exception {
        /*
        PLACE 0,0,NORTH
        MOVE
        REPORT
        Output: 0,1,NORTH;
        */
        String id = "/1"; // generated in the setup method
        // move
        mockMvc.perform(put(URI + id))
                .andExpect(status().isOk());
        // report
        mockMvc.perform(get(URI + id)
                .contentType(contentType))
                .andExpect(jsonPath("$.facing", is("NORTH")))
                .andExpect(jsonPath("$.x", is(0)))
                .andExpect(jsonPath("$.y", is(1)));

    }

    @Test
    public void testCase2() throws Exception {
        /*
        PLACE 0,0,NORTH
        LEFT
        REPORT
        Output: 0,0,WEST
        */
        String id = "/2"; // generated in the setup method
        // left
        mockMvc.perform(put(URI + "/turnleft" + id))
                .andExpect(status().isOk());
        // report
        mockMvc.perform(get(URI + id)
                .contentType(contentType))
                .andExpect(jsonPath("$.facing", is("WEST")))
                .andExpect(jsonPath("$.x", is(0)))
                .andExpect(jsonPath("$.y", is(0)));
    }

    @Test
    public void testCase3() throws Exception {
        /*
        PLACE 1,2,EAST
        MOVE
        MOVE
        LEFT
        MOVE
        REPORT
        Output: 3,3,NORTH
         */
        String id = "/3"; // generated in the setup method
        // move
        mockMvc.perform(put(URI + id))
                .andExpect(status().isOk());
        // move
        mockMvc.perform(put(URI + id))
                .andExpect(status().isOk());
        // left
        mockMvc.perform(put(URI + "/turnleft" + id))
                .andExpect(status().isOk());
        // move
        mockMvc.perform(put(URI + id))
                .andExpect(status().isOk());
        // report
        mockMvc.perform(get(URI + id)
                .contentType(contentType))
                .andExpect(jsonPath("$.facing", is("NORTH")))
                .andExpect(jsonPath("$.x", is(3)))
                .andExpect(jsonPath("$.y", is(3)));
    }

    @Test
    public void badRequestTest() throws Exception {
        String id = "/100"; // id 100 does not exist
        // move
        mockMvc.perform(put(URI + id))
                .andExpect(status().isBadRequest());;
    }


    @SuppressWarnings("unchecked")
    protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }

    private void postRequest(RobotDTO robotDTO) throws Exception {
        mockMvc.perform(post(URI)
                .content(this.json(robotDTO))
                .contentType(contentType))
                .andExpect(status().isCreated());
    }
}