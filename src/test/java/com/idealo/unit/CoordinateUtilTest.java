package com.idealo.unit;

import com.idealo.datatransferobject.RobotDTO;
import com.idealo.domainvalue.Facing;
import com.idealo.util.CoordinateUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CoordinateUtilTest {

    private RobotDTO robotDTO;

    @Before
    public void setup(){
        robotDTO = new RobotDTO();
        }

    @Test
    public void moveRobotTest(){
        robotDTO.setFacing(Facing.EAST);
        robotDTO.setX(0);
        robotDTO.setY(0);
        robotDTO = CoordinateUtil.moveRobot(robotDTO);
        Assert.assertEquals(1 , robotDTO.getX());
        Assert.assertEquals(0 , robotDTO.getY());
        Assert.assertEquals(Facing.EAST , robotDTO.getFacing());
    }

    @Test
    public void moveRobotOffTheGridTest(){
        robotDTO.setFacing(Facing.EAST);
        robotDTO.setX(4);
        robotDTO.setY(4);
        robotDTO = CoordinateUtil.moveRobot(robotDTO);
        Assert.assertEquals(4 , robotDTO.getX());
        Assert.assertEquals(4 , robotDTO.getY());
        Assert.assertEquals(Facing.EAST , robotDTO.getFacing());
    }

    @Test
    public void turnRobotLeftTest(){
        robotDTO.setFacing(Facing.EAST);
        robotDTO.setX(0);
        robotDTO.setY(0);
        robotDTO = CoordinateUtil.turnLeft(robotDTO);
        Assert.assertEquals(Facing.NORTH , robotDTO.getFacing());
    }

    @Test
    public void turnRobotRightTest(){
        robotDTO.setFacing(Facing.EAST);
        robotDTO.setX(0);
        robotDTO.setY(0);
        robotDTO = CoordinateUtil.turnRight(robotDTO);
        Assert.assertEquals(Facing.SOUTH , robotDTO.getFacing());
    }


}
