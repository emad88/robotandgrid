# README #

The project is based on the following technologies:

* Java 1.8
* Spring MVC with Spring Boot
* Maven
* Intellij as IDE 
* Lombok
* Swagger UI for documentation


### About the solution ###

* This app places a robot on a 5x5 grid and there are different functionalities  
* Place: will place the robot on the grid (post)
* move: moves the robot to the direction it's facing (put)
* left: turns the robot 90 degrees to the left of its current facing (put)
* right: turns the robot 90 degrees to the right of its current facing (put)
* report: report the current position and facing of the robot

* **NOTE:** when you place a robot on the grid using post, robot's id is returned in the "response header" in the location as a URL.

### How to run it? ###

* Clone the repository in your local pc, run the RobotandgridApplication
* browse http://localhost:8080 
* Open the robot-controller link in the UI and you will see all the exposed end-points 
* for simplicity you can also run the jar file in the jar folder with the command: java -jar robotandgrid.jar

### Side note for Lombok in Intellij ###

* Lombok should be enabled in intellij otherwise you will see errors:
* 1- Intellij IDEA -> Preferences -> Compiler -> Annotation Processors
  
* 2- File -> Other Settings -> Default Settings -> Compiler -> Annotation Processors
  
* 3- Intellij IDEA -> Preferences -> Plugins ->Browse Repositories-> Search for "Lombok"-> install plugin -> Apply and restart IDEA
  
* 4- Restart Intellij IDEA. 

